INTRODUCTION
------------

Provides a new shortcode that takes code snippets and displays them side by side to their rendered versions. One version displays the code ina  pre and code tag and the other displays it in a textarea for copying and pasting. Very usefull to show code snippets next to what their rendered versions are for things like style guides, link to us pages etc. This module requires the shortcode module.

REQUIREMENTS
------------
This module requires the following modules:
 * Shortcode (https://www.drupal.org/project/shortcode)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
Enable the new shortcodes "Dual Display code snippet and rendered version" and "Dual Display cut and paste snippet and rendered version" in the filter format that has shortcodes enabled (like filtered html or full html etc). Or create a new one.

Once enabled, use 

[code_display_dual title="" description=""]Code snippet with tags[/code_display_dual] 

[code_display_dual_cp title="" description=""]Code snippet with tags[/code_display_dual_cp] 

Title and Description appear above side by side versions of snippet and rendered code.
 
